package id.co.iconpln.controlflowapp.myUserForm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER = "extra_user"
        const val EXTRA_USER_EDIT = "extra_user_edit"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    private lateinit var user: UserDataResponse
    private lateinit var formViewModel: MyUserFormViewModel
    private lateinit var favoriteViewModel: FavoriteViewModel

    private var userId: Int? = null
    private var isEditUser = false

    private var isFavorite = false
    private var menuItem: Menu? = null

    private var favoriteUserId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        getIntentExtra()

        setClickListener()

    }

    private fun setClickListener() {
        btnUserFormSave.setOnClickListener(this)
        btnUserFormDelete.setOnClickListener(this)
        btnUserFormAdd.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_my_user_form, menu)
        menuItem = menu
//        setFavoriteIcon()
        if (!isEditUser) {
            menu?.findItem(R.id.actionFavorite)?.isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionFavorite -> {
                if (llMyUserFormContent.visibility == View.GONE) {
                    Toast.makeText(this, "Can't add to Favorite", Toast.LENGTH_SHORT).show()
                    return false
                }
                isFavorite = !isFavorite
                setFavoriteIcon()
                addOrRemoveFavorite()
                true
            }
            else -> true
        }
    }

    private fun setFavoriteIcon() {
        if (isFavorite) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
//            Toast.makeText(this, "Add to Favorite", Toast.LENGTH_SHORT).show()
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
//            Toast.makeText(this, "UnFavorite", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite) {
            addToFavorite()
        } else {
            removeFromFavorite()
        }
        favoriteViewModel.getAllFavoriteUsers().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                for (i in 0 until listFavUser.size) {
                    Log.d("restudiar", "${listFavUser[i].favUserId} -- ${listFavUser[i].userName}")
                }
            }
        })
    }

    private fun addToFavorite() {
        favoriteViewModel.insertUser(
            FavoriteUser(
                0,
                etUserFormAddress.text.toString(),
                userId.toString(),
                etUserFormName.text.toString(),
                etUserFormHp.text.toString()
            )
        )
    }

    private fun removeFromFavorite() {
        if (userId != null) {
            favoriteViewModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }

    private fun initViewModel() {
        formViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserFormViewModel::class.java)

        favoriteViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
                .get(FavoriteViewModel::class.java)
    }

    private fun getIntentExtra() {
        /*if (intent.hasExtra(EXTRA_USER)) {
            user = intent.getParcelableExtra(EXTRA_USER)
        } else {
            user = UserDataResponse("", 0, "", "")
        }*/
        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)

        checkForm(isEditUser)
    }

    private fun checkForm(editUser: Boolean) {
        if (!editUser) {
            btnUserFormSave.visibility = View.GONE
            btnUserFormDelete.visibility = View.GONE
            btnUserFormAdd.visibility = View.VISIBLE
            populateFormData(UserDataResponse("", 0, "", ""))
        } else {
            btnUserFormSave.visibility = View.VISIBLE
            btnUserFormDelete.visibility = View.VISIBLE
            btnUserFormAdd.visibility = View.GONE
            /*populateFormData(user)*/
            fetchUserData()
        }
    }

    private fun fetchUserData() {
        pbMyUserFormLoading.visibility = View.VISIBLE
        llMyUserFormContent.visibility = View.GONE
        getUser(userId as Int)
    }

    private fun populateFormData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)
        /*userId = user.id*/
        /*btnUserFormAdd.visibility = View.GONE
        btnUserFormSave.visibility = View.VISIBLE
        btnUserFormDelete.visibility = View.VISIBLE*/
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnUserFormSave -> {
                if (userId != null) {
                    val updateUserData = UserDataResponse(
                        etUserFormAddress.text.toString(), userId ?: 0,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                    )
                    updateUser(userId as Int, updateUserData)
                }
            }
            R.id.btnUserFormDelete -> {
                deleteUser(userId as Int)
            }
            R.id.btnUserFormAdd -> {
                userId = 0
                val addUserData = UserDataResponse(
                    etUserFormAddress.text.toString(), userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )
                addUser(addUserData)
            }
        }
    }

    private fun addUser(userData: UserDataResponse) {
        formViewModel.addUser(userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Created Succesfully", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to Create User", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun deleteUser(id: Int) {
        formViewModel.deleteUser(id).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Deleted Succesfully", Toast.LENGTH_SHORT).show()
                if (isFavorite) removeFromFavorite()
                finish()
            } else {
                Toast.makeText(this, "Failed to Delete User", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        formViewModel.updateUser(id, userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Updated Succesfully", Toast.LENGTH_SHORT).show()
                updateFavoriteUser(userDataResponse)
                finish()
            } else {
                Toast.makeText(this, "Failed to Update User", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateFavoriteUser(userDataResponse: UserDataResponse) {
        if (favoriteUserId != null) {
            favoriteViewModel.updateUser(
                FavoriteUser(
                    favoriteUserId as Long,
                    userDataResponse.address,
                    userDataResponse.id.toString(),
                    userDataResponse.name,
                    userDataResponse.phone
                )
            )
        }
    }

    private fun getUser(userId: Int) {
        formViewModel.getUser(userId).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null) {
                Toast.makeText(this, "User Loaded Succesfully", Toast.LENGTH_SHORT).show()
                populateFormData(userDataResponse)
                pbMyUserFormLoading.visibility = View.GONE
                llMyUserFormContent.visibility = View.VISIBLE
                setFavorite()
            } else {
                Toast.makeText(this, "Failed to Load User", Toast.LENGTH_SHORT).show()
                pbMyUserFormLoading.visibility = View.GONE
            }
        })
    }

    private fun setFavorite() {
        if (userId != null) {
            favoriteViewModel.getUser(userId as Int).observe(this, Observer { favoriteUser ->
                Log.d("restudiar", "getUser $favoriteUser")
                isFavorite = favoriteUser != null
                setFavoriteIcon()

                if (favoriteUser != null) {
                    favoriteUserId = favoriteUser.favUserId
                }
            })
        }
    }
}
