package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        initViewModel()
        showListMyUser()
        fetchMyUserData()
        addListClickListener()
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        fetchMyUserData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_favorite_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionFavoriteList) {
           startActivity(Intent(this, MyUserFavoriteActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.fabMyUserAdd -> {
                val addFormIntent = Intent(this, MyUserFormActivity::class.java)
                startActivity(addFormIntent)
            }
        }
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserViewModel::class.java)
    }

    private fun showListMyUser() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter
    }

    private fun fetchMyUserData() {
        myUserViewModel.getListUser().observe(this, Observer { myUserItem ->
            if (myUserItem != null) {
                adapter.setData(myUserItem)
            }
        })
    }

    private fun addListClickListener() {
        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback {
            override fun onItemClick(myUser: UserDataResponse) {
                val formIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
                /*formIntent.putExtra(MyUserFormActivity.EXTRA_USER, myUser)*/
                formIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, myUser.id)
                formIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
                startActivity(formIntent)
            }
        })
    }
}
