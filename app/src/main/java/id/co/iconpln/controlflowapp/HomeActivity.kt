package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.backgroundThread.BackgroundThreadActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreferences.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnHitung.setOnClickListener(this)
        btnKlasifikasi.setOnClickListener(this)
        btnHomeLogin.setOnClickListener(this)
        btnHomeOperation.setOnClickListener(this)
        btnStyle.setOnClickListener(this)
        btnDemo.setOnClickListener(this)
        btnVolume.setOnClickListener(this)
        btnHomeIntent.setOnClickListener(this)
        btnHomeComplexConstraint.setOnClickListener(this)
        btnHomeConstraint.setOnClickListener(this)
        btnHomeListHero.setOnClickListener(this)
        btnHomeGridHero.setOnClickListener(this)
        btnHomeDemoFragment.setOnClickListener(this)
        btnHomeTabFragment.setOnClickListener(this)
        btnHomeBottomNav.setOnClickListener(this)
        btnHomeNavDrawer.setOnClickListener(this)
        btnHomeBottomSheet.setOnClickListener(this)
        btnHomeLocalization.setOnClickListener(this)
        btnHomeScroll.setOnClickListener(this)
        btnHomeSharedPreferences.setOnClickListener(this)
        btnHomeWeather.setOnClickListener(this)
        btnHomeContact.setOnClickListener(this)
        btnHomeBackgroundThread.setOnClickListener(this)
        btnHomeContactTab.setOnClickListener(this)
        btnHomeMyContact.setOnClickListener(this)
        btnHomeMyUser.setOnClickListener(this)
        btnHomeMyProfileLogin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnHitung -> {
                val calculationIntent = Intent(this, MainActivity::class.java)
                startActivity(calculationIntent)
            }
            R.id.btnKlasifikasi -> {
                val classificationIntent = Intent(this, ClassificationActivity::class.java)
                startActivity(classificationIntent)
            }
            R.id.btnHomeLogin -> {
                val loginIntent = Intent(this, LoginActivity::class.java)
                startActivity(loginIntent)
            }
            R.id.btnHomeOperation -> {
                val operationIntent = Intent(this, OperationActivity::class.java)
                startActivity(operationIntent)
            }
            R.id.btnStyle -> {
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnDemo -> {
                val demoIntent = Intent(this, DemoActivity::class.java)
                startActivity(demoIntent)
            }
            R.id.btnVolume -> {
                val volumeIntent = Intent(this, VolumeActivity::class.java)
                startActivity(volumeIntent)
            }
            R.id.btnHomeIntent -> {
                val intentIntent = Intent(this, IntentActivity::class.java)
                startActivity(intentIntent)
            }
            R.id.btnHomeComplexConstraint -> {
                val complexConstraintIntent = Intent(this, ComplexConstraintActivity::class.java)
                startActivity(complexConstraintIntent)
            }
            R.id.btnHomeConstraint -> {
                val constraintIntent = Intent(this, ConstraintActivity::class.java)
                startActivity(constraintIntent)
            }
            R.id.btnHomeListHero -> {
                val listHeroIntent = Intent(this, ListHeroActivity::class.java)
                startActivity(listHeroIntent)
            }
            R.id.btnHomeGridHero -> {
                val gridHeroIntent = Intent(this, GridHeroActivity::class.java)
                startActivity(gridHeroIntent)
            }
            R.id.btnHomeDemoFragment -> {
                val demoFragmentIntent = Intent(this, DemoFragmentActivity::class.java)
                startActivity(demoFragmentIntent)
            }
            R.id.btnHomeTabFragment -> {
                val tabFragmentIntent = Intent(this, TabActivity::class.java)
                startActivity(tabFragmentIntent)
            }
            R.id.btnHomeBottomNav -> {
                val bottomNavIntent = Intent(this, BottomNavActivity::class.java)
                startActivity(bottomNavIntent)
            }
            R.id.btnHomeNavDrawer -> {
                val navDrawerIntent = Intent(this, NavDrawerActivity::class.java)
                startActivity(navDrawerIntent)
            }
            R.id.btnHomeBottomSheet -> {
                val bottomSheetIntent = Intent(this, BottomSheetActivity::class.java)
                startActivity(bottomSheetIntent)
            }
            R.id.btnHomeLocalization -> {
                val localizationIntent = Intent(this, LocalizationActivity::class.java)
                startActivity(localizationIntent)
            }
            R.id.btnHomeScroll -> {
                val scrollIntent = Intent(this, ScrollActivity::class.java)
                startActivity(scrollIntent)
            }
            R.id.btnHomeSharedPreferences -> {
                val sharedPreferencesIntent = Intent(this, SharedPreferencesActivity::class.java)
                startActivity(sharedPreferencesIntent)
            }
            R.id.btnHomeWeather -> {
                val weatherIntent = Intent(this, WeatherActivity::class.java)
                startActivity(weatherIntent)
            }
            R.id.btnHomeContact -> {
                val contactIntent = Intent(this, ContactActivity::class.java)
                startActivity(contactIntent)
            }
            R.id.btnHomeBackgroundThread -> {
                val backgroundThreadIntent = Intent(this, BackgroundThreadActivity::class.java)
                startActivity(backgroundThreadIntent)
            }
            R.id.btnHomeContactTab -> {
                val contactTabIntent = Intent(this, ContactTabActivity::class.java)
                startActivity(contactTabIntent)
            }
            R.id.btnHomeMyContact -> {
                val myContactIntent = Intent(this, MyContactActivity::class.java)
                startActivity(myContactIntent)
            }
            R.id.btnHomeMyUser -> {
                val myUserIntent = Intent(this, MyUserActivity::class.java)
                startActivity(myUserIntent)
            }
            R.id.btnHomeMyProfileLogin -> {
                startActivity(Intent(this, MyProfileActivity::class.java))
            }
        }
    }
}
