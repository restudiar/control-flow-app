package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLoginClick.setOnClickListener {
            val email = etLoginEmail.text.toString()
            val password = etLoginPassword.text.toString()
            if (email == "user@mail.com" && password == "password") {
                tvLoginConfirm.text = "Status Login : Login Berhasil"
            } else cekEmpty()
        }

        //TODO 1: username & password tidak boleh empty
        //TODO 2: password minimal 7 digit
        //TODO 3: sukses jika login: user@mail.com, password
        //TODO 4: username cek format email
    }

    fun cekEmpty() {
        val email = etLoginEmail.text.toString()
        val password = etLoginPassword.text.toString()
        if (email.isBlank()) {
            etLoginEmail.setError("Tidak Boleh Kosong")
        } else if (password.isBlank()) {
            etLoginPassword.setError("Tidak Boleh Kosong")
        } else if (password.length < 7) {
            etLoginPassword.setError("Tidak Boleh Kurang dari 7 Karakter")
        } else if (!EMAIL_ADDRESS.matcher(email).matches()) {
            etLoginEmail.setError("Format Email Salah")
        }
    }
}
