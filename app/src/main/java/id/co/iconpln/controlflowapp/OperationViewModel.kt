package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class OperationViewModel : ViewModel() {

    var operationResult: Int = 0
    var operation: String = "+"

    fun execute(x: Int, operation: Operation) {
        when (operation) {
            is Operation.Add -> operationResult = operation.value + x
            is Operation.Divide -> operationResult = operation.value / x
            is Operation.Multiply -> operationResult = operation.value * x
            is Operation.Substract -> operationResult = operation.value - x
        }
    }
}