package id.co.iconpln.controlflowapp.myContact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_my_contact.*

class MyContactActivity : AppCompatActivity() {

    private lateinit var adapter: MyContactAdapter
    private lateinit var myContactViewModel: MyContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_contact)

        initViewModel()
        showListMyContact()

        fetchMyContactData()
    }

    private fun initViewModel() {
        myContactViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyContactViewModel::class.java)
    }

    private fun showListMyContact() {
        adapter = MyContactAdapter()
        adapter.notifyDataSetChanged()

        rvMyContactList.layoutManager = LinearLayoutManager(this)
        rvMyContactList.adapter = adapter
    }

    private fun fetchMyContactData() {
        myContactViewModel.getListContact().observe(this, Observer { myContactItem ->
            if (myContactItem != null) {
                adapter.setData(myContactItem)
            }
        })
    }
}
