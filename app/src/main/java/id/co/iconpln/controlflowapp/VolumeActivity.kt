package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*

class VolumeActivity : AppCompatActivity() {

    lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        initViewModel()
        displayResult()
        setClickListener()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun displayResult() {
        tvVolumeResult.text = volumeViewModel.volumeResult.toString()
    }

    private fun setClickListener() {
        btnVolumeCalculate.setOnClickListener {
            val length = etVolumeLength.text.toString()
            val width = etVolumeWidth.text.toString()
            val height = etVolumeHeight.text.toString()

            when {
                length.isEmpty() -> {
                    etVolumeLength.error = "Empty Field"
                }
                width.isEmpty() -> {
                    etVolumeWidth.error = "Empty Field"
                }
                height.isEmpty() -> {
                    etVolumeHeight.error = "Empty Field"
                }
                else -> {
                    volumeViewModel.calculate(length, width, height)
                    displayResult()
                }
            }
        }
    }
}