package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*

class GridHeroActivity : AppCompatActivity() {

    private var listHero: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setupGridHero()
        showRecyclerGrid()
    }

    private fun setupGridHero() {
        rvGridHero.setHasFixedSize(true)
        listHero.addAll(HeroesData.listDataHero)
    }


    private fun showRecyclerGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 2)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvGridHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemClickCallback(object : GridHeroAdapter.OnItemClickCallback {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, "Pahlawan ${hero.name}", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}
