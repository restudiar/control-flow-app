package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var operationViewModel: OperationViewModel

    private var inputX: Int = 0
    private var inputY: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()
        etBilanganX.setText("0")
        etBilanganY.setText("0")

    }

    private fun displayResult() {
        tvOperationResult.text = operationViewModel.operationResult.toString()
        tvOperator.text = operationViewModel.operation
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun getInputNumbers() {
        if (etBilanganX.text?.isNotEmpty() == true || etBilanganY.text?.isNotEmpty() == true) {
            inputX = etBilanganX.text.toString().toInt()
            inputY = etBilanganY.text.toString().toInt()
        }
    }

    private fun setButtonClickListener() {
        btnOperationAdd.setOnClickListener(this)
        btnOperationSubstract.setOnClickListener(this)
        btnOperationMultiply.setOnClickListener(this)
        btnOperationDivide.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnOperationAdd -> {
                operationViewModel.operation = getString(R.string.operation_add)
                getInputNumbers()
                val add = Operation.Add(inputX)
                operationViewModel.execute(inputY, add)
                displayResult()
            }
            R.id.btnOperationSubstract -> {
                operationViewModel.operation = getString(R.string.operation_substract)
                getInputNumbers()
                val substract = Operation.Substract(inputX)
                operationViewModel.execute(inputY, substract)
                displayResult()
            }
            R.id.btnOperationMultiply -> {
                operationViewModel.operation = getString(R.string.operation_multiply)
                getInputNumbers()
                val multiply = Operation.Multiply(inputX)
                operationViewModel.execute(inputY, multiply)
                displayResult()
            }
            R.id.btnOperationDivide -> {
                operationViewModel.operation = getString(R.string.operation_divide)
                getInputNumbers()
                if (inputY == 0) {
                    Toast.makeText(this, "Pembagi tidak boleh 0", Toast.LENGTH_LONG).show()
                } else {
                    val divide = Operation.Divide(inputX)
                    operationViewModel.execute(inputY, divide)
                    displayResult()
                }
            }
            R.id.btnReset -> {
                etBilanganX.setText("0")
                etBilanganY.setText("0")
                operationViewModel.operation = "+"
                operationViewModel.operationResult = 0
                displayResult()
            }
        }
    }
}
