package id.co.iconpln.controlflowapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnIntentMove.setOnClickListener(this)
        btnIntentData.setOnClickListener(this)
        btnIntentBundle.setOnClickListener(this)
        btnIntentObject.setOnClickListener(this)
        btnIntentImplicit.setOnClickListener(this)
        btnIntentWeb.setOnClickListener(this)
        btnIntentSms.setOnClickListener(this)
        btnIntentMap.setOnClickListener(this)
        btnIntentShareText.setOnClickListener(this)
        btnIntentResult.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnIntentMove -> {
                val moveIntent = Intent(this, StyleActivity::class.java)
                startActivity(moveIntent)
            }
            R.id.btnIntentData -> {
                val dataIntent = Intent(this, IntentDataActivity::class.java)
                dataIntent.putExtra(IntentDataActivity.EXTRA_NAME, "restudiar")
                dataIntent.putExtra(IntentDataActivity.EXTRA_AGE, 99)
                startActivity(dataIntent)
            }
            R.id.btnIntentBundle -> {
                val bundleIntent = Intent(this, IntentBundleActivity::class.java)
                val bundle = Bundle()
                bundle.putString(IntentBundleActivity.EXTRA_BUNDLE_NAME, "restudiar")
                bundle.putInt(IntentBundleActivity.EXTRA_BUNDLE_AGE, 69)
                bundleIntent.putExtras(bundle)
                startActivity(bundleIntent)
            }
            R.id.btnIntentObject -> {
                val person = Person("restudiar", 10, "abc@mail.com", "Smg")
                val objectIntent = Intent(this, IntentObjectActivity::class.java)
                objectIntent.putExtra(IntentObjectActivity.EXTRA_PERSON, person)
                startActivity(objectIntent)
            }
            R.id.btnIntentImplicit -> {
                val phoneNumber = "0812345678999"
                val dialPhoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phoneNumber"))
                if (dialPhoneIntent.resolveActivity(packageManager) != null) {
                    startActivity(dialPhoneIntent)
                }
            }
            R.id.btnIntentWeb -> {
                val webPage = Uri.parse("https://gitlab.com/")
                val openWebIntent = Intent(Intent.ACTION_VIEW, webPage)
                if (openWebIntent.resolveActivity(packageManager) != null) {
                    startActivity(openWebIntent)
                }
            }
            R.id.btnIntentSms -> {
                val phoneNumber = "085323661116"
                val sendSms = Uri.parse("smsto: $phoneNumber")
                val message = "Nyoba SMS"
                val sendSmsIntent = Intent(Intent.ACTION_SENDTO, sendSms)
                sendSmsIntent.putExtra("sms_body", message)
                if (sendSmsIntent.resolveActivity(packageManager) != null) {
                    startActivity(sendSmsIntent)
                }
            }
            R.id.btnIntentMap -> {
                val latitude = "-7.7228421"
                val longitude = "110.3904715"
                val showMap = Uri.parse("geo: $latitude,$longitude")
                val showMapIntent = Intent(Intent.ACTION_VIEW, showMap)
                if (showMapIntent.resolveActivity(packageManager) != null) {
                    startActivity(showMapIntent)
                }
            }
            R.id.btnIntentShareText -> {
                val sharedText = "Nyoba Share Text"
                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type = "text/plain"
                val shareIntent = Intent.createChooser(shareTextIntent, null)
                if (shareIntent.resolveActivity(packageManager) != null) {
                    startActivity(shareIntent)
                }
            }
            R.id.btnIntentResult -> {
                val resultIntent = Intent(this, IntentResultActivity::class.java)
                startActivityForResult(resultIntent, REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (resultCode == IntentResultActivity.RESULT_CODE) {
                val selectedValue = data?.getIntExtra(IntentResultActivity.EXTRA_VALUE, 0)
                tvIntentResult.text = "Hasilnya $selectedValue"
            }
        }
    }
}