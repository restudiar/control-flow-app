package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*
import kotlinx.android.synthetic.main.activity_main.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        etClassificationNilai.setText("0")
        btnClassificationShow.setOnClickListener {
            if (etClassificationNilai.text.isNullOrEmpty()) {
                Toast.makeText(this,"Tidak Boleh Kosong", Toast.LENGTH_LONG).show()
            }
            else if (etClassificationNilai.text.toString().toInt() >1000) {
                Toast.makeText(this,"Tidak Boleh Lebih dari 1000", Toast.LENGTH_LONG).show()
            }
            else doClassification(etClassificationNilai.text.toString().toInt())
        }
    }

    fun doClassification(nilai: Int) {

        //TODO Nilai 0-70 Tidak lulus
        //TODO Nilai 71-80 Lulus
        //TODO Nilai 81-100 Lulus Sempurna
        //TODO selain diatas Nilai Error
        //input empty, tidak boleh crash
        //nilai maksimal yang bisa diinput 1000

        when (nilai) {
            in 0..70 -> tvClassificationHasil.text = "Maaf Anda Belum Lulus"
            in 71..80 -> tvClassificationHasil.text = "Selamat Anda Lulus"
            in 81..100 -> tvClassificationHasil.text = "Anda Lulus Sempurna"
            else -> tvClassificationHasil.text = "Input Nilai Error"
        }
    }
}
